import psycopg2
import pandas as pd
from dateutil.parser import parse
from datetime import date, datetime, timedelta
import os

# -------------------------------------------------------------------------------------------------------------------------------------------------

path = '/home/jake/AirQuality/Temp/toronto-air-quality.csv'
cityName = 'Toronto'


currentYearMonth = (2021, 3)
# -------------------------------------------------------------------------------------------------------------------------------------------------

os.chdir(os.path.dirname(os.path.realpath(__file__)))
os.chdir("..")

# read db password
pwFile = open("password.txt", "r")
password = pwFile.read()
password = password.rstrip()
pwFile.close()

# connect to postgres
conn = psycopg2.connect(
    host = "localhost",
    database = "airquality",
    user = "jake",
    password = password)

cur = conn.cursor()

df = pd.read_csv(path, header = 0, names = ['date', 'pm25'], usecols = ['date', 'pm25'])

monthly = dict()

for c, d in df.iterrows():
	if d['pm25'] != ' ':
		cur.execute("insert into airQualityDaily (city, date, pm25) values (%s, %s, %s) on conflict do nothing;", (cityName, d['date'], d['pm25']))
		
		p = parse(d['date'])

		key = (int(p.strftime("%Y")), int(p.strftime("%m")))
		if key in monthly:
			monthly[key].append(int(d['pm25']))
		else:
			monthly[key] = [int(d['pm25'])]

for k in monthly:
	if k != currentYearMonth:
		pm25Sum = sum(monthly[k])
		pm25Avg = pm25Sum / len(monthly[k])
		cur.execute("insert into airQualityMonthly (city, year, month, pm25) values (%s, %s, %s, %s) on conflict do nothing;", (cityName, k[0], k[1], pm25Avg))

conn.commit()
cur.close()
conn.close()

