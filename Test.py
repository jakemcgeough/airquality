from dateutil.parser import parse
from datetime import date, datetime, timedelta
import os
import psycopg2
import pandas as pd

os.chdir(os.path.dirname(os.path.realpath(__file__)))

pwFile = open("../password.txt", "r")
pw = pwFile.read()
pw = pw.strip()
pwFile.close()

conn = psycopg2.connect(host = "localhost", database = "airquality", user = "jake", password = pw)

cur = conn.cursor()

cur.execute("select date from airQualityDaily order by date desc limit 1;", conn)
latestDate = cur.fetchone()[0]
dailyLimitTime = latestDate + timedelta(days = -90)

df = pd.read_sql("select * from airQualityDaily where date > %s order by date;", conn, params = [dailyLimitTime])

print(df)
