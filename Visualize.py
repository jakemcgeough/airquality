import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import plotly.express as px
import pandas as pd

import psycopg2

from dateutil.parser import parse
from datetime import date, datetime, timedelta

import os

# cities need to be the same across all tables

# bars to adjust timeline
# all button (and none button if necessary)
# continents - add Sydney, somewhere in Antarctica?? - NA, SA, Asia, Europe, Africa, Australia, Antarctica


# standard deviation
# what other stats?


os.chdir(os.path.dirname(os.path.realpath(__file__)))
# os.chdir("..")

# read db password
pwFile = open("../password.txt", "r")
pw = pwFile.read()
pw = pw.rstrip()
pwFile.close()

# connect to postgres
conn = psycopg2.connect(
    host = "localhost",
    database = "airquality",
    user = "jake",
    password = pw)

cur = conn.cursor()

# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
# app = dash.Dash(__name__, external_stylesheets = external_stylesheets)

app = dash.Dash(__name__)
server = app.server

app.title = "Air Quality - PM 2.5"

cur.execute("select absdatetime from airQualityHourly order by absdatetime desc limit 1;")
latestDateTime = cur.fetchone()[0]
hourlyLimitTime = latestDateTime + timedelta(days = -14)
dfHourly = pd.read_sql("select * from airQualityHourly where absdatetime > %s;", conn, params = [hourlyLimitTime])
dfHourly.sort_values(by = 'absdatetime', inplace = True)

cur.execute("select date from airQualityDaily order by date desc limit 1;", conn)
latestDate = cur.fetchone()[0]
dailyLimitTime = latestDate + timedelta(days = -90)
dfDaily = pd.read_sql("select * from airQualityDaily where date > %s;", conn, params = [dailyLimitTime])
dfDaily.sort_values(by = 'date', inplace = True)

dfMonthly = pd.read_sql("select * from airQualityMonthly;", conn)
dfMonthly['year-month'] = dfMonthly['year'].astype(str) + '-' + dfMonthly['month'].astype(str)
dfMonthly.sort_values(by = ['year', 'month'], inplace = True)



# cities need to be same in all db tables
allCities = dfDaily.city.unique()
allCities.sort()

app.layout = html.Div([
	html.H4(children = 'Jake McGeough'),
	html.H1(children = 'Air Quality - PM 2.5'),
	html.Div(className = 'div-dropdown', children = [
		dcc.Dropdown(
	        id = 'dropdown',
	        options = [
	            {'label': 'Hourly', 'value': 'Hourly'},
	            {'label': 'Daily', 'value': 'Daily'},
	            {'label': 'Monthly', 'value': 'Monthly'}
	        ],
	        value = 'Hourly',
	        clearable = False
	    )]
	),
	html.Div(className = 'div-checklist', children = [
	    dcc.Checklist(
	    	id = 'citySelect',
	    	labelClassName = 'label-checklist',
			options = [{'label': x, 'value': x} for x in allCities],
			value = ['Vancouver'],
			labelStyle = {'display': 'inline-block'}
		)]
	),
	html.Div(className = 'div-graph', children = [
	    dcc.Graph(
	        id = 'airQualityOverTime'
	    )]
    ),
	html.Div(className = 'about', children = [
    	html.P("PM 2.5 is particulate matter with diameter of less than or equal to 2.5 micrometers."),
	html.P("Users can select Hourly, Daily, or Monthly data, and can choose to display data from one or more cities."),
	html.P("Data is updated continuously as it becomes available."),
    	html.P("Source: World Air Quality Index Project")]
    )
])


# need to have at least one city selected when switching from hourly to daily or vv
@app.callback(
	Output('airQualityOverTime', 'figure'),
	Input('citySelect', 'value'),
	Input('dropdown', 'value'))
def UpdateGraph(cities, timeframe):
	if timeframe == 'Hourly':
		dfHourly = pd.read_sql("select * from airQualityHourly where absdatetime > %s;", conn, params = [hourlyLimitTime])
		dfHourly.sort_values(by = 'absdatetime', inplace = True)
		df = dfHourly
		df['Datetime (GMT)'] = df['absdatetime']
		mask = df.city.isin(cities)
		fig = px.line(df[mask], x = "Datetime (GMT)", y = "pm25", color = "city")
	elif timeframe == 'Daily':
		dfDaily = pd.read_sql("select * from airQualityDaily where date > %s;", conn, params = [dailyLimitTime])
		dfDaily.sort_values(by = 'date', inplace = True)
		df = dfDaily
		mask = df.city.isin(cities)
		fig = px.line(df[mask], x = "date", y = "pm25", color = "city")
	elif timeframe == 'Monthly':
		dfMonthly = pd.read_sql("select * from airQualityMonthly;", conn)
		dfMonthly['year-month'] = dfMonthly['year'].astype(str) + '-' + dfMonthly['month'].astype(str)
		dfMonthly.sort_values(by = ['year', 'month'], inplace = True)
		df = dfMonthly
		mask = df.city.isin(cities)
		fig = px.line(df[mask], x = "year-month", y = "pm25", color = "city")

	return fig


if __name__ == '__main__':
    app.run_server(debug = True)

