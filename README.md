![airquality](airQualityPic.png)

This project monitors air quality from large cities around the world. Users can select which cities to view and can adjust the timeframe.

Data is continuously updated via a Python script which pulls data from an API and updates data in PostgreSQL.

Finally the data is graphed using Python packages dash-plotly and pandas.
