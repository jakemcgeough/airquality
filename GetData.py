import psycopg2
import requests
from multiprocessing import Pool
from dateutil.parser import parse
from datetime import date, datetime, timedelta
import os

# no rio de janeiro
# no cairo
# no manila PM 2.5
# no kinshasa

# no tokyo, nagoya, osaka

# https://github.com/psf/requests/issues/4023

def callApi(city, token):
	apiPath = "https://api.waqi.info/feed/" + city + "/?token=" + token
	openRequest = requests.get(apiPath)
	data = openRequest.json()

	try:
		return city, data["data"]["time"]["s"], data["data"]["time"]["tz"], data["data"]["iaqi"]["pm25"]["v"]
	except KeyError:
		pass


if __name__ == '__main__':

	os.chdir(os.path.dirname(os.path.realpath(__file__)))
	os.chdir("..")

	# read db password
	pwFile = open("password.txt", "r")
	pw = pwFile.read()
	pw = pw.rstrip()
	pwFile.close()

	# connect to postgres
	conn = psycopg2.connect(
	    host = "localhost",
	    database = "airquality",
	    user = "jake",
	    password = pw)

	cur = conn.cursor()

	# read api token
	apiTokenFile = open("ApiToken.txt", "r")
	apiToken = apiTokenFile.read()
	apiToken = apiToken.rstrip()
	apiTokenFile.close()

	# read list of cities
	citiesFile = open("Cities.txt", "r")
	cities = citiesFile.read().splitlines()
	citiesFile.close()

	# print("cities:", len(cities))

	# api calls for all cities (multiprocessing)
	with Pool(35) as p:
		args = [(city, apiToken) for city in cities]
		currentResult = p.starmap(callApi, args)

		for r in currentResult:

			if r == None:
				print("Missing pm25")
				continue

			# calculate absolute datetime
			localDateTime = parse(r[1])
			timezoneHours = -1 * int(r[2].split(':')[0])
			timezoneMinutes = int(r[2].split(':')[1])
			if r[2][0] == '+':
				timezoneMinutes *= -1
			# print(localDateTime)
			# print(r[2], timezoneHours, timezoneMinutes)
			absTime = localDateTime + timedelta(hours = timezoneHours)
			absTime = absTime + timedelta(minutes = timezoneMinutes)
			# print(absTime)
			# print("\n")

			# if data is more than a month old, discard:
			cutoffTime = datetime.now() + timedelta(days = -30)

			if absTime >= cutoffTime:
				cur.execute("insert into airQualityHourly(city, date, time, absdatetime, pm25) values (%s, %s, %s, %s, %s) on conflict do nothing;", \
				(r[0], r[1].split(' ')[0], r[1].split(' ')[1], absTime, r[3]))
	
			previousDate = localDateTime.date() + timedelta(days = -1)

			cur.execute("select count(*) from airQualityDaily where city = %s and date = %s;", (r[0], previousDate))
			previousDateCount = cur.fetchone()[0]
			# print("prev date count:", previousDateCount)
			if previousDateCount == 0:
				cur.execute("select pm25 from airQualityHourly where city = %s and date = %s;", (r[0], previousDate))
				pm25Raw = cur.fetchall()
				pm25List = [i[0] for i in pm25Raw]
				if len(pm25List) > 0:
					pm25Sum = sum(pm25List)
					pm25Avg = round(pm25Sum / len(pm25List))
					
					cur.execute("insert into airQualityDaily(city, date, pm25) values (%s, %s, %s);", (r[0], previousDate, pm25Avg))
					print("Daily inserted for:", r[0])


			previousMonth = int(localDateTime.strftime("%m")) - 1
			if previousMonth == 12:
				relevantYear = int(localDateTime.strftime("%Y")) - 1
			else:
				relevantYear = int(localDateTime.strftime("%Y"))
			# print(relevantYear)

			cur.execute("select count(*) from airQualityMonthly where city = %s and year = %s and month = %s;", (r[0], relevantYear, previousMonth))
			previousMonthCount = cur.fetchone()[0]
			# print("prev month count:", previousMonthCount)

			if previousMonthCount == 0:
				previousMonthStart = date(relevantYear, previousMonth, 1)
				currentMonthStart = date(localDateTime.year, localDateTime.month, 1)
				# print("previousMonthStart:", previousMonthStart, "| currentMonthStart:", currentMonthStart)

				cur.execute("select pm25 from airQualityDaily where city = %s and date >= %s and date < %s;", (r[0], previousMonthStart, currentMonthStart))
				dailyRaw = cur.fetchall()
				dailyList = [i[0] for i in dailyRaw]
				if len(dailyList) > 0:
					dailySum = sum(dailyList)
					dailyAvg = round(dailySum / len(dailyList))

					cur.execute("insert into airQualityMonthly(city, year, month, pm25) values (%s, %s, %s, %s);", (r[0], relevantYear, previousMonth, dailyAvg))
					print("- Monthly inserted for:", r[0])



	# commit changes and close connection to postgres
	conn.commit()
	cur.close()
	conn.close()

